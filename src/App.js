import React, {Component} from 'react';
import './App.css';
import { Grid, Row, Col } from 'react-bootstrap';
import ShowDone from './components/show-done';
import SearchControl from './components/search-control';
import ProgressLine from './components/progress-bar';
import AddCategoryControl from './components/add-category-control';
import EditCategory from './components/edit-category';
import TaskList from './components/task-list';
import CategoryList from './components/category-list';
import data from './data';


class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categories: data.categories,
            tasks: data.tasks,
            activeCatId: 0,
            currentTasks: [],
            activeTaskId: null,
            activeTask: null,
            showModal: false
        }
        this.clickCategoryItem = this.clickCategoryItem.bind(this);
        //this.editTask = this.editTask.bind(this, i);
    }

    clickCategoryItem(e) {
        const id = e.target.id;
        this.setState({
            activeCatId: id

        });
        this.showTasksByCatId(id);
    }

    showTasksByCatId(id) {
        let tasks = this.state.tasks.filter((item)=> +item.catId === +id);
        this.setState({
            currentTasks: tasks
        });
    }
    editTask(e) {
        let id = e.target.getAttribute("data-target-id");
        this.setState({
            activeTaskId: id,
            showModal: true,
            activeTask: this.state.tasks.find((item => +item.id === +id))
        });
    }

    closeModal() {
        this.setState({ showModal: false });
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col md={6}>
                        <span>To-Do List</span>
                    </Col>
                    <Col md={2}>
                        <ShowDone />
                    </Col>
                    <Col md={4}>
                        <SearchControl />
                    </Col>
                </Row>

                <Row>
                    <Col sm={12}>
                        <ProgressLine />
                    </Col>
                </Row>

                <Row>
                    <Col md={6}>
                        <AddCategoryControl />
                    </Col>
                </Row>
                <Row>
                    <Col sm={4}>
                        <CategoryList active={this.state.activeCatId} catList={this.state.categories}
                                      onItemClick={this.clickCategoryItem}/>
                    </Col>
                    <Col sm={8}>
                        <TaskList tasks={this.state.currentTasks} editHandler={this.editTask.bind(this)}/>
                    </Col>
                </Row>
                <EditCategory  showModal={this.state.showModal} closeModal={this.closeModal.bind(this)} task={this.state.activeTask}/>
            </Grid>
        );
    }
}

export default App;
