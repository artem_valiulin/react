import React, { Component } from 'react';
import { Modal, FormGroup, Checkbox, FormControl, Button, Row, Col } from 'react-bootstrap';

class EditCategory extends Component {
    
    render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit task</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Row>
                        <Col>
                            <FormGroup >
                                <FormControl type="text"
                                             label="Task title"
                                             placeholder="Enter Title"
                                             value={this.props.task?this.props.task.title: ''}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Checkbox >
                                Done
                            </Checkbox>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                                <FormControl componentClass="textarea" placeholder="Enter Description" />
                            </FormGroup>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.closeModal}>Close</Button>
                    <Button bsStyle="primary">Save changes</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default EditCategory;