import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import CategoryItem from './category-item';

class CategoryList extends Component {
    renderCats (){
        if (this.props.catList.length) {
            const cat = [];
            this.props.catList.forEach((item)=>{
                cat.push(
                    <CategoryItem id={item.id} title={item.title} active={this.props.active} onClick={this.props.onItemClick}/>
                );
            });
            
            return <ListGroup>{cat}</ListGroup>;
        } else {
            return null;
        }
    }

    render() {
        return this.renderCats();
    }
}


export default CategoryList;