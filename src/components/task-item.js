import React, { Component } from 'react';
import { ListGroupItem, Checkbox, Button, Glyphicon } from 'react-bootstrap';

class TaskItem extends Component {
    
    render() {
        let editIdAttr = {
            'data-target-id': this.props.id
        };
        
        return (
            <ListGroupItem className="task-item">
                <Checkbox checked>
                    {this.props.title}
                </Checkbox>
                <span className="task-item-controls">
                    <Button 
                        bsSize="xsmall" 
                        className="task-item-control" 
                        onClick={this.props.editHandler}
                        
                        {...editIdAttr}>
                        <Glyphicon glyph="edit" />
                        
                    </Button>
                </span>
            </ListGroupItem>
        );
    }
}

export default TaskItem;