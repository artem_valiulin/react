import React, { Component } from 'react';
import { InputGroup, FormGroup, FormControl, Button } from 'react-bootstrap';

class SearchControl extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }
    render() {
        return (
            <FormGroup>
                <InputGroup>
                    <FormControl type="text" placeholder="Search"/>
                    <InputGroup.Button>
                        <Button>Submit</Button>
                    </InputGroup.Button>
                </InputGroup>
            </FormGroup>
        );
    }
}
export default SearchControl;