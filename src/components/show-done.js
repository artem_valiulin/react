import React, { Component } from 'react';
import { Checkbox, FormGroup } from 'react-bootstrap';

class ShowDone extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }
    render() {
        return (
            <FormGroup>
                <Checkbox checked>
                    Show done
                </Checkbox>
            </FormGroup>
        );
    }
}
export default ShowDone;