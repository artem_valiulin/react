import React, { Component } from 'react';
import { FormGroup, InputGroup, FormControl, Button } from 'react-bootstrap';

class AddCategoryControl extends Component {
    constructor(props){
        super(props)
        this.state = {
            progress: 10
        }
    }
    render() {
        return (
            <FormGroup>
                <InputGroup>
                    <FormControl type="text" placeholder="Enter category title"/>
                    <InputGroup.Button>
                        <Button>Add</Button>
                    </InputGroup.Button>
                </InputGroup>
            </FormGroup>
        );
    }
}

export default AddCategoryControl;