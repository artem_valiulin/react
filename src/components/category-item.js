import React, { Component } from 'react';
import { ListGroupItem, Button, Glyphicon } from 'react-bootstrap';

class CategoryItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            isActive: +props.active === +props.id
        };
    }

    render() {
        return (
            <ListGroupItem onClick={this.props.onClick}
                className={"category-item " + (+this.props.active === +this.props.id ? 'active' : '')}
                id={this.props.id}
            >
                {this.props.title}
                <span className="category-item-controls">
                    <Button className="category-item-control" bsSize="xsmall">
                        <Glyphicon glyph="edit" />
                    </Button>
                    <Button className="category-item-control" bsSize="xsmall">
                        <Glyphicon glyph="trash" />
                    </Button>
                    <Button className="category-item-control" bsSize="xsmall">
                        <Glyphicon glyph="plus" />
                    </Button>
                </span>
            </ListGroupItem>
        );
    }
}

export default CategoryItem;