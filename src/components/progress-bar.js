import React, { Component } from 'react';
import { ProgressBar } from 'react-bootstrap';

class ProgressLine extends Component {
    constructor(props){
        super(props)
        this.state = {
            progress: 10
        }
    }
    render() {
        return (
            <ProgressBar striped now={this.state.progress} />
        );
    }
}

export default ProgressLine;