import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import TaskItem from './task-item';

class TaskList extends Component {
    renderTasks (){
        if (this.props.tasks.length) {
            const tasks = [];
            this.props.tasks.forEach((item)=>{
                tasks.push(
                    <TaskItem 
                        title={item.title} 
                        id={item.id} 
                        editHandler={this.props.editHandler}/>
                );
            });

            return <ListGroup>{tasks}</ListGroup>;
        } else {
            return null;
        }
    }

    render() {
        return this.renderTasks();
    }

}

export default TaskList;