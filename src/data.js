export default {
    categories: [
        {
            id: 1,
            title: "First category _1"
        },
        {
            id: 2,
            title: "Second category _2"
        },
        {
            id: 3,
            title: "Third category _3"
        }
    ],
    tasks: [
        {
            id: 0,
            title: "Task 1 1",
            catId: 1
        },
        {
            id: 1,
            title: "Task 1 2",
            catId: 1
        },
        {
            id: 2,
            title: "Task 1 3",
            catId: 1
        },
        {
            id: 3,
            title: "Task 2 1",
            catId: 2
        },
        {
            id: 4,
            title: "Task 2 2",
            catId: 2
        },
        {
            id: 5,
            title: "Task 2 3",
            catId: 2
        },
        {
            id: 6,
            title: "Task 3 1",
            catId: 3
        },
        {
            id: 7,
            title: "Task 3 2",
            catId: 3
        },
        {
            id: 8,
            title: "Task 3 3",
            catId: 3
        },
        {
            id: 9,
            title: "Task 2 4",
            catId: 2
        },
        {
            id: 10,
            title: "Task 2 5",
            catId: 2
        },
        {
            id: 11,
            title: "Task 3 4",
            catId: 3
        }
    ]
};